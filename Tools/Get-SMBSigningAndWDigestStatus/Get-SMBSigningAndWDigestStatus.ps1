#Requires -Version 1.0

<#
    .Synopsis
    Check status of SMB Signing and WDigest
    
    .DESCRIPTION
    Check status of SMB Signing and WDigest using REG QUERY to support all Powershell versions
    
    .EXAMPLE
    .\Get-SMBSigningAndWDigestStatus.ps1
    
    .EXAMPLE
    Output to a custom location

    .\Get-SMBSigningAndWDigestStatus.ps1 -SplunkOutputFolder C:\Temp\ -OutputToSplunk Yes

    .EXAMPLE
    Output to a custom location and only output csv file

    .\Get-SMBSigningAndWDigestStatus.ps1 -OutputToCSV C:\Temp\ -OutputToSplunk No -SplunkOutputFolder Yes
#>

[CmdletBinding()]
Param
(
  $CSVOutputFolder,
  $SplunkOutputFolder,
  $RemoveFilesOlderThanDays = 30,

  [ValidateSet('Yes', 'No')]
  [Parameter()]
  $OutputToSplunk = 'Yes',

  [ValidateSet('Yes', 'No')]
  [Parameter()]
  $OutputToCSV = 'No'
)

$TimeStamp = (Get-Date).ToString('yyyyMMddHHmmss')
$Date = (Get-Date).ToString('yyyyMMdd')
$CSVOutputFile = Join-Path $CSVOutputFolder "$Env:COMPUTERNAME.csv"
$SplunkOutputFile = Join-Path $SplunkOutputFolder "$($Date)_SMBSigningAndWDigestStatus.txt"

# Creating the output folder - if needed
if (!(Test-Path $CSVOutputFolder)) { $null | New-Item -Path $CSVOutputFolder -ItemType Directory }
if (!(Test-Path $SplunkOutputFolder)) { $null | New-Item -Path $SplunkOutputFolder -ItemType Directory }

#region Get the status of SMB signing

$SMBParam = reg.exe QUERY HKLM\SYSTEM\CurrentControlSet\Services\LanmanServer\Parameters
$enablesecuritysignature = (($SMBParam | Select-String -Pattern 'enablesecuritysignature') -split ' ')[-1]
$requiresecuritysignature = (($SMBParam | Select-String -Pattern 'requiresecuritysignature') -split ' ')[-1]

if ($enablesecuritysignature -like '0x1') {
  $SMBSigningEnabled = 'YES' 
}
else {
  $SMBSigningEnabled = 'NO' 
}

if ($requiresecuritysignature -like '0x1') {
  $SMBSigningRequired = 'YES' 
}
else {
  $SMBSigningRequired = 'NO' 
}

#endregion Get the status of SMB signing

#region Get the status of WDigest

$WDigestParam = reg.exe QUERY HKLM\SYSTEM\CurrentControlSet\Control\SecurityProviders\WDigest
$UseLogonCredential = (($WDigestParam | Select-String -Pattern 'UseLogonCredential') -split ' ')[-1]

if ($UseLogonCredential -like '0x1') {
  $WDigestEnabled = 'YES' 
}
else {
  $WDigestEnabled = 'NO' 
}

#endregion Get the status of WDigest 

#region Result
$result = New-Object -TypeName PSObject
$result | Add-Member -MemberType NoteProperty -Name Checked -Value (Get-Date -Format "dd.MM.yyyy HH.mm.ss")
$result | Add-Member -MemberType NoteProperty -Name Server -Value $Env:COMPUTERNAME
$result | Add-Member -MemberType NoteProperty -Name SMBSigningEnabled -Value $SMBSigningEnabled
$result | Add-Member -MemberType NoteProperty -Name SMBSigningRequired -Value $SMBSigningRequired
$result | Add-Member -MemberType NoteProperty -Name WDigestEnabled -Value $WDigestEnabled
$result | Add-Member -MemberType NoteProperty -Name OSVersion -Value ((Get-Item "HKLM:\SOFTWARE\Microsoft\Windows NT\CurrentVersion").GetValue('ProductName'))

$Stoploop = $false
$retries = 3
[int]$Retrycount = 0
do {
  try {

    if ($OutputToCSV -eq 'Yes') {
      $result | Export-Csv -Path $CSVOutputFile -Delimiter ';' -NoTypeInformation -Encoding UTF8
    }
    if ($OutputToSplunk -eq 'Yes') {
      "$TimeStamp SERVER=$($result.Server) ISSUE=SMBSIGNING ENABLED=$($result.SMBSigningEnabled) REQUIRED=$($result.SMBSigningRequired) OS=`"$($result.OSVersion)`"
$TimeStamp SERVER=$($result.Server) ISSUE=WDIGEST ENABLED=$($result.WDigestEnabled) OS=`"$($result.OSVersion)`"" | Out-File $SplunkOutputFile -Append
    }
    
    $Stoploop = $true
  }
  catch {
    if ($Retrycount -eq $retries) {
      Write-Output "ERROR : Could not do the job after $retries retries. `n$($_.Exception.Message)"
      $Stoploop = $true
    }
    else {
      $Retrycount++
      Write-Output "WARNING : $Retrycount/$retries : $($_.Exception.Message) `nRetrying in 30 seconds..."
      Start-Sleep -Seconds 30
    }
  }
} While ($Stoploop -eq $false)



#endregion Result

# House cleaning
Get-ChildItem -Path $OutputFolder -Include "*SMBSigningAndWDigestStatus*" -Recurse | 
Where-Object CreationTime -lt (Get-Date).AddDays(-$RemoveFilesOlderThanDays) | 
Remove-Item -Force -ErrorAction SilentlyContinue