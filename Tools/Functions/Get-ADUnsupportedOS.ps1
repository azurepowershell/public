function Get-ADUnsupportedOS {
    <#
    .Synopsis
    Get unsupported OS based on info in AD

    .DESCRIPTION
    List all computer accounts with outdated OS
    The Build number must be with only digits (no dots). See examples below

    .EXAMPLE
    Get-ADUnsupportedOS -OU 'OU=Clients,DC=Mydomain,DC=local' -MinimumClientBuildNumber 10018363

    .EXAMPLE
    Get-ADUnsupportedOS -OU 'DC=Mydomain,DC=local' -MinimumServerBuildNumber 629600  -MinimumClientBuildNumber 10018363 | Out-GridView

#>

    [CmdletBinding()]
    param (
        [Parameter(Mandatory = $true)]
        [string]$OU,

        [int]$MinimumClientBuildNumber,

        [int]$MinimumServerBuildNumber
    )
    
    Begin {
        $result = @()
    }

    Process {
                
        if ($MinimumClientBuildNumber) {
            $Objects = Get-ADComputer -Filter "Enabled -eq 'True' -and OperatingSystem -notlike '*server*'" -SearchBase $OU `
                -Properties OperatingSystem, PasswordLastSet, OperatingSystemVersion, whenChanged, whenCreated, LastLogonDate |
            Where-Object OperatingSystem -match 'Windows'
        }

        if ($MinimumServerBuildNumber) {
            $Objects += Get-ADComputer -Filter "Enabled -eq 'True' -and OperatingSystem -like 'Windows*server*'" -SearchBase $OU `
                -Properties OperatingSystem, PasswordLastSet, OperatingSystemVersion, whenChanged, whenCreated, LastLogonDate
        }

        foreach ($Object in $Objects) {

            if ($Object.OperatingSystemVersion -match '[A-Za-z]') {
                $OperatingSystemVersion = 0
            } 
            else {
                [int]$OperatingSystemVersion = ($Object.OperatingSystemVersion -replace '\(|\)|\.|\s', '')
            }
                
                
            if ($OperatingSystemVersion -lt $MinimumServerBuildNumber) {
                $result += [pscustomobject]@{
                    Name                = $Object.Name
                    FQDN                = $Object.DNSHostName
                    'OS Name'           = $Object.OperatingSystem
                    'Build Number'      = $Object.OperatingSystemVersion
                    Enabled             = $Object.Enabled
                    Changed             = $Object.whenChanged
                    Created             = $Object.whenCreated
                    'Last Logon Date'   = $Object.LastLogonDate
                    'Password Last Set' = $Object.PasswordLastSet
                }  
            }
        }
    }
        
                    
    end {
        # Output the result
        $result
    }
}
