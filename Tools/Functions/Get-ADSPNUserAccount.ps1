
function Get-ADSPNUserAccount {
    <#
    .Synopsis
    Get SPN for user account

    .DESCRIPTION
    List all user accounts with SPN

    .EXAMPLE
    Get-ADSPNUserAccount -OU 'OU=Service Accounts,DC=Mydomain,DC=local' | where 'Unconstrained Delegation'

    .EXAMPLE
    Get-ADSPNUserAccount -Username User1

#>

    [CmdletBinding()]
    param (
        [Parameter(ParameterSetName = 'OU', Mandatory = $true)]
        [string]$OU,

        [Parameter(ParameterSetName = 'User', Mandatory = $true)]
        [string]$Username
    )
    
    Begin {

        switch ($PSCmdlet.ParameterSetName) {
            OU {
                $SPNs = Get-ADUser -SearchBase $OU -Filter * -Properties ServicePrincipalName, TrustedForDelegation, TrustedToAuthForDelegation | 
                Where-Object ServicePrincipalName  
            }
            User {
                $SPNs = Get-ADUser -Identity $Username -Properties ServicePrincipalName, TrustedForDelegation, TrustedToAuthForDelegation | 
                Where-Object ServicePrincipalName  
            }
        }

 
        $SPNCommandString = @'
($SPNs | where ServicePrincipalName -eq $SPN).name
'@
        $TrustedForDelegationCommandString = @'
($SPNs | where ServicePrincipalName -eq $SPN).TrustedForDelegation
'@
        $TrustedToAuthForDelegationCommandString = @'
($SPNs | where ServicePrincipalName -eq $SPN).TrustedToAuthForDelegation
'@
        
        $result = @()
        
    }

    Process {
        
        foreach ($SPN in $SPNs.ServicePrincipalName) {

            $result += [PSCustomObject]@{
                Name                       = Invoke-Expression -Command $SPNCommandString
                SPN                        = $SPN
                'Unconstrained Delegation' = Invoke-Expression -Command $TrustedForDelegationCommandString
                'Constrained Delegation'   = Invoke-Expression -Command $TrustedToAuthForDelegationCommandString
            }
        }        
    }

    End {
        # Output the result
        $result
    }
}

