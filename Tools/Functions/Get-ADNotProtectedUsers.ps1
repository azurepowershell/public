
function Get-ADNotProtectedUsers {
    <#
    .Synopsis
    List all admin accounts that are not member of Protected Users

    .DESCRIPTION
    List all admin accounts that are not member of Protected Users

    .EXAMPLE
    Get-ADNotProtectedUsers

    .EXAMPLE
    Get-ADNotProtectedUsers | Out-GridView

#>

    [CmdletBinding()]
    param (
        [Parameter(Mandatory = $true)]
        [ValidateSet('Yes', 'No')]
        $ListOnlySensitive
    )
    
    
    Begin {

        $result = @()
        $SensitiveGroups = @(
            "Domain Admins", 
            "Enterprise Admins", 
            "Schema Admins",
            "Account Operators", 
            "Backup Operators", 
            "Print Operators", 
            "Server Operators", 
            "Group Policy Creator Owners"
        )
        
    }

    Process {
        foreach ($group in $SensitiveGroups) {
            $SensitiveUsers = Get-ADGroupMember -Identity $group -Recursive | Where-Object SamAccountName -NotMatch '\$$' | Select-Object SamAccountName
            $result = $SensitiveUsers | 
            ForEach-Object { Get-ADUser $_.SamAccountName -Properties MemberOf, LastLogonDate | 
                Where-Object MemberOf -NotMatch 'Protected Users' | 
                Select-Object Name, SamAccountName, UserPrincipalName, Enabled, LastLogonDate
            }

            if ($ListOnlySensitive -eq 'No') {
                $result += Get-ADUser -Filter "SamAccountName -like 'adm_*'" -Properties MemberOf, LastLogonDate | 
                Where-Object MemberOf -NotMatch 'Protected Users' | 
                Select-Object Name, SamAccountName, UserPrincipalName, Enabled, LastLogonDate
            }
        }

    }

    End {
        # Output the result
        $result = $result | Select-Object -Unique Name, SamAccountName, UserPrincipalName, Enabled, LastLogonDate
        $result
    }
}




