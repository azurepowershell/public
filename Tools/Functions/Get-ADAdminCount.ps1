
function Get-ADAdminCount {
    <#
    .Synopsis
    List all users who has had a membership to protected groups

    .DESCRIPTION
    List all users where AdminCount is set to 1

    .EXAMPLE
    Get-ADAdminCount

    .EXAMPLE
    Get-ADAdminCount -OU 'OU=Users,DC=Mydomain,DC=local'
#>

    [CmdletBinding()]
    param (
        [string]$OU
    )
    
    Begin {

    }

    Process {
        if ($OU) {
            $result = Get-ADUser -SearchBase $OU -Filter "AdminCount -eq 1 -and Enabled -eq 'True'" -Properties AdminCount
        }
        else {
            $result = Get-ADUser -Filter "AdminCount -eq 1 -and Enabled -eq 'True'" -Properties AdminCount
        }
    }

    End {
        # Output the result
        $result = $result | Select-Object Name, SamAccountName, UserPrincipalName, AdminCount, Enabled
        $result
    }
}




