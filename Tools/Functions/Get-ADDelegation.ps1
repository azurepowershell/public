
function Get-ADDelegation {
    <#
    .Synopsis
    List all computer account or user accounts with Unconstrained Delegation or Constrained Delegation

    .DESCRIPTION
    List all computer account or user accounts with Unconstrained Delegation or Constrained Delegation

    .EXAMPLE
    Get-ADDelegation -AccountType User

    .EXAMPLE
    Get-ADDelegation -AccountType Computer

#>

    [CmdletBinding()]
    param (
        [Parameter(Mandatory = $true)]
        [ValidateSet('User', 'Computer')]
        [string]$AccountType
    )
    
    Begin {
    }

    Process {
        if ($AccountType -eq 'User') {
            $result = Get-ADUser -Filter "(TrustedForDelegation -eq 'true' -or TrustedToAuthForDelegation -eq 'true')" -Properties TrustedForDelegation, TrustedToAuthForDelegation |
            Where-Object DistinguishedName -NotMatch 'Domain Controller' | Select-Object Name, SamAccountName, UserPrincipalName, Trusted*, Enabled
        } 
        
        if ($AccountType -eq 'Computer') {
            $result = Get-ADComputer -Filter "(TrustedForDelegation -eq 'true' -or TrustedToAuthForDelegation -eq 'true')" -Properties TrustedForDelegation, TrustedToAuthForDelegation |
            Where-Object DistinguishedName -NotMatch 'Domain Controller' | Select-Object Name, DNSHostName, Trusted*, Enabled
        }
        
    }

    End {
        # Output the result
        $result
    }
}




