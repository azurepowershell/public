
function Get-ADSensitiveGroupMember {
    <#
    .Synopsis
    List all members of sensitive groups

    .DESCRIPTION
    List all members of sensitive groups

    .EXAMPLE
    Get-ADSensitiveGroupMember

    .EXAMPLE
    Get-ADSensitiveGroupMember | Out-GridView

#>

    [CmdletBinding()]
    param ()
    
    
    Begin {

       $result = @()
        $SensitiveGroups = @(
            "Domain Admins", 
            "Enterprise Admins", 
            "Schema Admins",
            "Account Operators", 
            "Backup Operators", 
            "Print Operators", 
            "Server Operators", 
            "Group Policy Creator Owners"
        )
        
    }

    Process {
       foreach ($group in $SensitiveGroups) {
           $result += Get-ADGroupMember -Identity $group -Recursive | 
            Select-Object @{Name = 'Member'; Expression = {$_.Name}}, @{Name = 'Group'; Expression = {$group}}
       }
    }

    End {
        # Output the result
        $result
    }
}




