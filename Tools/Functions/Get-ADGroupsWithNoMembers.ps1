function Get-ADGroupsWithNoMembers {
    <#
    .Synopsis
    Get AD Groups without members

    .DESCRIPTION
    List all AD groups that do not have any members

    .EXAMPLE
    Get-ADGroupsWithNoMembers -OU 'OU=Groups,DC=Mydomain,DC=local'

    .EXAMPLE
    Get-ADGroupsWithNoMembers -OU 'OU=Groups,DC=Mydomain,DC=local' | Export-Csv -NoTypeInformation -Delimiter ';' -Path c:\temp\nomembers.csv

    .EXAMPLE
    Only groups in THAT OU

    Get-ADGroupsWithNoMembers -OU 'OU=Groups,DC=Mydomain,DC=local' -SearchScope OneLevel

    .EXAMPLE
    Get all groups in the subtree (recursive)

    Get-ADGroupsWithNoMembers -OU 'OU=Groups,DC=Mydomain,DC=local' -SearchScope Subtree

    .EXAMPLE
    Only showing the numbers

    Get-ADGroupsWithNoMembers -OU 'OU=Groups,DC=Mydomain,DC=local' -OnlyStatistic Yes

#>

    [CmdletBinding()]
    param (
        [Parameter(Mandatory = $true)]
        [string]$OU,

        [Parameter(Mandatory = $false)]
        [ValidateSet('OneLevel', 'Subtree')]
        $SearchScope = 'Subtree',

        [Parameter(Mandatory = $false)]
        [ValidateSet('Yes', 'No')]
        [string]$OnlyStatistic = 'No'
    )
    
    Begin {
               
    }

    Process {
        $ADGroups = Get-ADGroup -SearchBase $OU -Filter * -Properties Members -SearchScope $SearchScope
        $result = $ADGroups  | Where-Object { -not $_.members }  
    }

    End {
        # Output the result
        if ($OnlyStatistic -eq 'Yes') {
            Write-Host "`n`nAD Groups membership statistic" -ForegroundColor Magenta
            Write-Host " - OU              : $OU" -ForegroundColor Blue
            Write-Host " - Search Scoupe   : $SearchScope`n" -ForegroundColor Blue
            Write-Host " - Without members : $($result.count)" -ForegroundColor Blue
            Write-Host " - With members    : $($ADGroups.count - $result.count)" -ForegroundColor Blue
            Write-Host " - Total           : $($ADGroups.count)`n`n" -ForegroundColor Blue
        }
        else {
            $result = $result | Select-Object Name, DistinguishedName, GroupCategory, GroupScope, ObjectGUID, SamAccountName, SID
            $result
        }
    }
}
