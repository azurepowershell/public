function Get-ADStaleAccount {
    <#
    .Synopsis
    Get stale AD Accounts

    .DESCRIPTION
    List all accounts that has not been used since XXX

    .EXAMPLE
    Get-ADStaleAccount -LastLoginAgeInDays 365 -OU 'OU=Users,DC=Mydomain,DC=local' -AccountType User

#>

    [CmdletBinding()]
    param (
        [Parameter(Mandatory = $true)]
        [string]$OU,

        [Parameter(Mandatory = $true)]
        [int]$LastLoginAgeInDays,

        [Parameter(Mandatory = $true)]
        [ValidateSet('User', 'Computer')]
        [string]$AccountType
    )
    
    Begin {
        $PasswordAge = (Get-Date).AddDays(-$LastLoginAgeInDays)
        
    }

    Process {
        
        if ($AccountType -eq 'User') {
            $result = Get-ADUser -Filter "Enabled -eq 'True'" -SearchBase $OU -Properties PasswordLastSet, LastLogonDate, whenCreated, whenChanged | 
            Where-Object LastLogonDate -lt $PasswordAge | 
            Select-Object Name, SamaccountName, UserPrincipalName, LastLogonDate, PasswordLastSet, Enabled, whenCreated, whenChanged
        } 
        
        if ($AccountType -eq 'Computer') {
            $result = Get-ADComputer -Filter "Enabled -eq 'True'" -SearchBase $OU -Properties PasswordLastSet, LastLogonDate, whenCreated, whenChanged | 
            Where-Object LastLogonDate -lt $PasswordAge | 
            Select-Object Name, DNSHostName, LastLogonDate, PasswordLastSet, Enabled, whenCreated, whenChanged
        }
        
    }

    End {
        # Output the result
        $result
    }
}
