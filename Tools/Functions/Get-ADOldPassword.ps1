function Get-ADOldPassword {
    <#
    .Synopsis
    Get accounts with old password

    .DESCRIPTION
    List all accounts with password older then $MaxPasswordAgeInDays

    .EXAMPLE
    .\Get-ADOldPassword -MaxPasswordAgeInDays 365 -OU 'OU=Servers,DC=Mydomain,DC=local'

#>

    [CmdletBinding()]
    param (
        [Parameter(Mandatory = $true)]
        [string]$OU,

        [Parameter(Mandatory = $true)]
        [int]$MaxPasswordAgeInDays
    )
    
    Begin {
        
        $PasswordAge = (Get-Date).AddDays(-$MaxPasswordAgeInDays)  
    }
    process {   

        $Result = Get-ADUser -Filter "Enabled -eq 'True'" -SearchBase $OU -Properties PasswordLastSet, LastLogonDate, whenCreated, whenChanged | 
        Where-Object PasswordLastSet -lt $PasswordAge |
        Select-Object Name, LastLogonDate, SamaccountName, UserPrincipalName, PasswordLastSet, Enabled, whenCreated, whenChanged     
    }
    end {
        # Output the result
        $result
    }
}
