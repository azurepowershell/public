<#
  .Synopsis
  Create HTML reports for all linked GPOs to in a OU subtree

  .DESCRIPTION
  Create HTML reports for all linked GPOs to in a OU subtree.
  You only need to specify the OU and the folder where you want the HTML reports to be created.

  .EXAMPLE
  .\Create-GPOHTMLReport.ps1 -OU 'OU=Domain Controllers,DC=nhy,DC=hydro,DC=com'-OutputFolder 'C:\temp\DomainControllersGPOs' -searchScope Subtree

  .PARAMETER OU
  The OU you are targeting

  .PARAMETER OUTPPUTFOLDER
  The folder where you will put the HTML reports

  .PARAMETER SEARCHSCOPE
  How deap will you go? Base, Onelevel or Subtree?
  
  .NOTES
  Author        : Stian Hammer Sæten
  Creation Date : 13/02/2020
#>

[CmdletBinding()]
Param(
  # The OU in Scope
  [Parameter(Mandatory = $true)]
  [String]$OU,

  [Parameter(Mandatory = $true)] 
  [String]$OutputFolder,

  [Parameter(Mandatory = $true)] 
  [ValidateSet('Subtree', 'Base', 'Onelevel')]
  [String]$SearchScope
)

#Creating the Output folder if it is missing
$null = New-Item -Path $OutputFolder -ItemType Directory -ErrorAction SilentlyContinue

#Collect all link GPOs to the specified OU
$LinkedGPOs = Get-ADOrganizationalUnit -Filter * -SearchBase $OU -SearchScope $SearchScope |
Select-Object -ExpandProperty LinkedGroupPolicyObjects
$LinkedGPOGUIDs = $LinkedGPOs | ForEach-Object { $_.Substring(4, 36) }
$LinkedGPONames = ($LinkedGPOGUIDs | ForEach-Object { Get-GPO -Guid $_ }).Displayname

#Creating the report for each GPO
Write-Host "GPO reports will be put in $OutputFolder :`n" -ForegroundColor Yellow

foreach ($GPO in $LinkedGPONames) {
  Write-Host " - Collecting $GPO..." -ForegroundColor Green
  Get-GPOReport -Name $GPO -ReportType Html -Path ('{0}\{1}.html' -f $OutputFolder, $GPO).Replace('*', '')
}