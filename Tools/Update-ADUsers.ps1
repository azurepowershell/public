<#
.Synopsis
   Update AD users based on a csv file
.DESCRIPTION
   It will dynamically check all attributes in the csv file with the AD object based on the SamAccountName,
   and update the attributes where it is not correct 
.EXAMPLE
   .\Update-ADUsers.ps1 -CSVinput c:\temp\users.csv
#>


[CmdletBinding()]
Param
(
    [String] $CSVinput
)


$csvusers = Import-Csv -Delimiter ';'  -Path $CSVinput
$attributes = ($csvusers | get-member -type NoteProperty).name 

foreach ($csvuser in $csvusers) {
    try {
        $aduser = Get-ADUser $csvuser.samaccountname -Properties *
    }
    catch {
        Write-Host "Error : $($csvuser.samaccountname) not found" -ForegroundColor Red
        continue # skip to the next in foreach loop
    }
    
    foreach ($attribute in $attributes) {
        if ($csvuser.$attribute -notlike $aduser.$attribute) {
            try {
                switch ($attribute) {
                    Enabled { 
                        $enabled = [System.Convert]::ToBoolean('{0}' -f $csvuser.enabled)
                        Set-ADUser $aduser -Enabled $enabled
                    }
                    Name {
                        Rename-ADObject $aduser -NewName $csvuser.name
                    }
                    Default { 
                        Try { Set-ADUser $aduser -Replace @{$attribute = $csvuser.$attribute } }
                        catch { Write-Verbose "$($aduser.samaccountname) : Unable to use 'Set-ADUser $aduser -Replace @{$attribute = $csvuser.$attribute' and will use 'Set-ADUser $aduser "-$attribute" $csvuser.$attribute'" }
                        Try { Set-ADUser $aduser "-$attribute" $csvuser.$attribute } 
                        catch { Write-Verbose "$($aduser.samaccountname) : Used 'Set-ADUser $aduser "-$attribute" $csvuser.$attribute'" }    
                    }
                }
                Write-Host "$($aduser.samaccountname) : Updating : $attribute from '$($aduser.$attribute)' to '$($csvuser.$attribute)'"  
            }
            catch {
                Write-Host "$($aduser.samaccountname) : Error : Update $attribute from '$($aduser.$attribute)' to '$($csvuser.$attribute)'" -ForegroundColor Red
                continue # skip to the next in foreach loop
            }
            
        }
        else {
            Write-Verbose "$($aduser.samaccountname) : Skipping : $attribute is correct"
        }
    }
}
